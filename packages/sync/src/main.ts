import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { InsightlyService } from './insightly/insightly.service'

async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule, {
    logger: (process.env.NODE_ENV !== 'production' || process.env.DEBUG) ? ['error', 'warn', 'log', 'debug', 'verbose'] : ['error', 'warn', 'log'],
  });
  const insightly = app.get(InsightlyService)

  await insightly.import()

  await app.close()
}
bootstrap();
