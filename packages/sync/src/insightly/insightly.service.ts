import { Injectable, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Contact, ContactDocument } from './schemas/contact.schema'
import { Model } from 'mongoose'
import { HttpService } from '@nestjs/axios'
import { ContactDto } from './dtos/Contact.dto'
import { NoteDto } from './dtos/Note.dto'
import { Note, NoteDocument } from './schemas/note.schema'
import { plainToInstance } from 'class-transformer'
import { Log, LogDocument, Result } from './schemas/log.schema'

@Injectable()
export class InsightlyService {
  private readonly TOTAL_COUNT_HEADER = 'x-total-count' as const

  private readonly logger = new Logger(InsightlyService.name)

  constructor(
    private readonly httpService: HttpService,
    @InjectModel(Log.name) private logModel: Model<LogDocument>,
    @InjectModel(Contact.name) private contactModel: Model<ContactDocument>,
    @InjectModel(Note.name) private noteModal: Model<NoteDocument>,
  ) {
  }

  public async import() {
    const mostRecentJob = await this.logModel.findOne({
      endDate: { $ne: null },
    }, null, {
      sort: { endDate: -1 },
    }).exec()

    const log = await this.logModel.create({
      startDate: new Date(),
    })

    await log.save()

    log.results = await Promise.all([
      this.importContacts(mostRecentJob?.startDate),
      this.importNotes(mostRecentJob?.startDate),
    ])

    log.endDate = new Date()

    await log.save()
  }

  public async importContacts(since: Date = new Date(0)): Promise<Result> {
    const res: Result = { inserts: 0, updates: 0, skips: 0 }
    const perPage = 500
    let page = 0
    let remaining = Number.POSITIVE_INFINITY
    let fetched = 0

    this.logger.verbose(`[Contacts] Updating contacts since ${since.toISOString()}`)

    do {
      const bulk = this.contactModel.collection.initializeUnorderedBulkOp()
      const { data, headers } = await this.httpService.axiosRef.get<ContactDto[]>('/Contacts', {
        params: {
          top: perPage,
          skip: page++ * perPage,
          brief: false,
          count_total: true,
        },
      })

      fetched += data.length
      remaining = Number(headers[this.TOTAL_COUNT_HEADER]) - fetched

      for (const contact of data.map(d => plainToInstance(ContactDto, d))) {
        if (contact.DATE_UPDATED_UTC <= since) {
          res.skips++
          continue
        }

        bulk.find({
          'CONTACT_ID': { $eq: contact.CONTACT_ID },
        }).upsert().updateOne({ $set: contact })
      }

      this.logger.verbose(`[Contacts] Fetched ${fetched} of ${headers[this.TOTAL_COUNT_HEADER]} (${remaining} remaining)`)

      if (bulk.batches.length > 0) {
        const { matchedCount, upsertedCount } = await bulk.execute()

        res.inserts += upsertedCount
        res.updates += matchedCount
      }
    } while (remaining > 0)

    this.logger.log(`[Contacts] Updated: ${res.updates}, Inserted: ${res.inserts}, Skipped: ${res.skips}`)

    return res
  }

  public async importNotes(since: Date = new Date(0)): Promise<Result> {
    const res: Result = { inserts: 0, updates: 0, skips: 0 }
    const perPage = 500
    let page = 0
    let remaining = Number.POSITIVE_INFINITY
    let fetched = 0

    this.logger.verbose(`[Notes] Updating notes since ${since.toISOString()}`)

    do {
      const bulk = this.noteModal.collection.initializeUnorderedBulkOp()
      const { data, headers } = await this.httpService.axiosRef.get<NoteDto[]>('/Notes', {
        params: {
          top: perPage,
          skip: page++ * perPage,
          brief: false,
          count_total: true,
        },
      })

      fetched += data.length
      remaining = Number(headers[this.TOTAL_COUNT_HEADER]) - fetched

      for (const note of data.map(d => plainToInstance(NoteDto, d))) {
        if (note.DATE_UPDATED_UTC <= since) {
          res.skips++
          continue
        }

        bulk.find({
          'NOTE_ID': { $eq: note.NOTE_ID },
        }).upsert().updateOne({ $set: note })
      }

      this.logger.verbose(`[Notes] Fetched ${fetched} of ${headers[this.TOTAL_COUNT_HEADER]} (${remaining} remaining)`)

      if (bulk.batches.length > 0) {
        const { matchedCount, upsertedCount } = await bulk.execute()

        res.inserts += upsertedCount
        res.updates += matchedCount
      }
    } while (remaining > 0)

    this.logger.log(`[Notes] Updated: ${res.updates}, Inserted: ${res.inserts}, Skipped: ${res.skips}`)

    return res
  }
}
