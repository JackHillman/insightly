import { Logger, Module } from '@nestjs/common'
import { InsightlyService } from './insightly.service';
import { MongooseModule } from '@nestjs/mongoose'
import { Contact, ContactSchema } from './schemas/contact.schema'
import { HttpModule } from '@nestjs/axios'
import { Note, NoteSchema } from './schemas/note.schema'
import { Log, LogSchema } from './schemas/log.schema'


@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        baseURL: 'https://api.na1.insightly.com/v3.1',
        headers: {
          Authorization: `Basic ${Buffer.from(process.env.INSIGHTLY_API_KEY).toString('base64')}`
        }
      })
    }),
    MongooseModule.forFeature([
      {
        name: Contact.name,
        schema: ContactSchema,
      },
      {
        name: Note.name,
        schema: NoteSchema,
      },
      {
        name: Log.name,
        schema: LogSchema,
      },
    ]),
  ],
  providers: [InsightlyService]
})
export class InsightlyModule {}
