import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose';

export type ContactDocument = Contact & Document;

@Schema()
export class Contact {
  @Prop({ isRequired: true, unique: true })
  CONTACT_ID: number

  @Prop()
  SALUTATION: string | null

  @Prop()
  FIRST_NAME: string

  @Prop()
  LAST_NAME: string

  @Prop()
  IMAGE_URL: string | null

  @Prop()
  BACKGROUND: string | null

  @Prop()
  OWNER_USER_ID: number

  @Prop(Date)
  DATE_CREATED_UTC: Date

  @Prop(Date)
  DATE_UPDATED_UTC: Date

  @Prop()
  SOCIAL_LINKEDIN: string | null

  @Prop()
  SOCIAL_FACEBOOK: string | null

  @Prop()
  SOCIAL_TWITTER: string | null

  @Prop()
  DATE_OF_BIRTH: string | null

  @Prop()
  PHONE: string | null

  @Prop()
  PHONE_HOME: string | null

  @Prop()
  PHONE_MOBILE: string | null

  @Prop()
  PHONE_OTHER: string | null

  @Prop()
  PHONE_ASSISTANT: string | null

  @Prop()
  PHONE_FAX: string | null

  @Prop()
  EMAIL_ADDRESS: string | null

  @Prop()
  ASSISTANT_NAME: string | null

  @Prop()
  ADDRESS_MAIL_STREET: string | null

  @Prop()
  ADDRESS_MAIL_CITY: string | null

  @Prop()
  ADDRESS_MAIL_STATE: string | null

  @Prop()
  ADDRESS_MAIL_POSTCODE: string | null

  @Prop()
  ADDRESS_MAIL_COUNTRY: string | null

  @Prop()
  ADDRESS_OTHER_STREET: string | null

  @Prop()
  ADDRESS_OTHER_CITY: string | null

  @Prop()
  ADDRESS_OTHER_STATE: string | null

  @Prop()
  ADDRESS_OTHER_POSTCODE: string | null

  @Prop()
  ADDRESS_OTHER_COUNTRY: string | null

  @Prop()
  LAST_ACTIVITY_DATE_UTC: string | null

  @Prop()
  NEXT_ACTIVITY_DATE_UTC: string | null

  @Prop()
  CREATED_USER_ID: number | null

  @Prop()
  ORGANISATION_ID: number | null

  @Prop()
  TITLE: string | null

  @Prop()
  EMAIL_OPTED_OUT: boolean

  @Prop({
    type: [raw({
      FIELD_NAME: { type: String },
      FIELD_VALUE: { type: String },
      CUSTOM_FIELD_ID: { type: String },
    })],
  })
  CUSTOMFIELDS: Array<{
    FIELD_NAME: string
    FIELD_VALUE: string
    CUSTOM_FIELD_ID: string | null
  }>

  @Prop({
    type: [raw({
      TAG_NAME: { type: String },
    })],
  })
  TAGS: Array<{
    TAG_NAME: string
  }>

  @Prop({
    type: [raw({
      DATE_ID: { type: Number },
      OCCASION_NAME: { type: String },
      OCCASION_DATE: { type: String },
      REPEAT_YEARLY: { type: Boolean },
      CREATE_TASK_YEARLY: { type: Boolean },
    })],
  })
  DATES: Array<{
    DATE_ID: number
    OCCASION_NAME: string | null
    OCCASION_DATE: string
    REPEAT_YEARLY: boolean
    CREATE_TASK_YEARLY: boolean
  }>

  @Prop({
    type: [raw({
      LINK_ID: { type: Number },
      OBJECT_NAME: { type: String },
      OBJECT_ID: { type: Number },
      LINK_OBJECT_NAME: { type: String },
      LINK_OBJECT_ID: { type: Number },
      ROLE: { type: String },
      DETAILS: { type: String },
      RELATIONSHIP_ID: { type: Number },
      IS_FORWARD: { type: Boolean },
    })],
  })
  LINKS: Array<{
    LINK_ID: number
    OBJECT_NAME: string
    OBJECT_ID: number
    LINK_OBJECT_NAME: string
    LINK_OBJECT_ID: number
    ROLE: string
    DETAILS: string | null
    RELATIONSHIP_ID: number
    IS_FORWARD: boolean
  }>
}

export const ContactSchema = SchemaFactory.createForClass(Contact);