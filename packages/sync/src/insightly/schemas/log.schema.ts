import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose';

export type ResultDocument = Result & Document

@Schema()
export class Result {
  @Prop()
  inserts: number

  @Prop()
  updates: number

  @Prop()
  skips: number
}

export const ResultSchema = SchemaFactory.createForClass(Result);

export type LogDocument = Log & Document;

@Schema()
export class Log {
  @Prop({ type: Date, index: true })
  startDate: Date

  @Prop({ type: Date, index: true })
  endDate: Date

  @Prop({
    type: [ResultSchema],
    default: [],
  })
  results: Result[] = []
}

export const LogSchema = SchemaFactory.createForClass(Log);
