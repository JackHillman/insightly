import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import { Contact } from './contact.schema'

export type NoteDocument = Note & Document;

@Schema()
export class Note {
  @Prop({ isRequired: true, unique: true })
  NOTE_ID: number

  @Prop()
  TITLE: string

  @Prop()
  BODY: string

  @Prop(Date)
  DATE_CREATED_UTC: Date

  @Prop(Date)
  DATE_UPDATED_UTC: Date

  @Prop()
  OWNER_USER_ID: number

  @Prop()
  VISIBLE_TO: 'EVERYONE' | string

  @Prop()
  VISIBILE_TEAM_ID: number | null

  @Prop({
    type: [raw({
      LINK_ID: { type: Number },
      OBJECT_NAME: { type: String },
      OBJECT_ID: { type: Number },
      LINK_OBJECT_NAME: { type: String },
      LINK_OBJECT_ID: { type: Number },
      ROLE: { type: String },
      DETAILS: { type: String },
      RELATIONSHIP_ID: { type: Number },
      IS_FORWARD: { type: Boolean },
    })],
  })
  LINKS: Array<{
    LINK_ID: number
    OBJECT_NAME: 'Note'
    OBJECT_ID: number
    LINK_OBJECT_NAME: 'Contact' | string
    LINK_OBJECT_ID: number
    ROLE: string
    DETAILS: string | null
    RELATIONSHIP_ID: number
    IS_FORWARD: boolean
  }>
}

export const NoteSchema = SchemaFactory.createForClass(Note);

