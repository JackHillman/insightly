import { Contact } from '../schemas/contact.schema'
import { Transform } from 'class-transformer'

export class ContactDto extends Contact {
  @Transform(({ value }) => new Date(value))
  DATE_CREATED_UTC: Date

  @Transform(({ value }) => new Date(value))
  DATE_UPDATED_UTC: Date
}