import { Note } from '../schemas/note.schema'
import { Transform } from 'class-transformer'

export class NoteDto extends Note {
  @Transform(({ value }) => new Date(value))
  DATE_CREATED_UTC: Date

  @Transform(({ value }) => new Date(value))
  DATE_UPDATED_UTC: Date
}